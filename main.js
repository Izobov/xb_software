

class CityMap {
    constructor (cities) {
        this.cities=this.parser(cities)
        let StorageCities = JSON.parse(localStorage.getItem('cities'))
        if(StorageCities != null){
            let concat = [...this.cities, ...StorageCities]
            let arr =[]
            concat.sort()
            arr.push(concat[0])
            for( let i=1; i<concat.length; i++){
                
                if(concat[i][0]!=concat[i-1][0]){
                    
                    arr.push(concat[i])
                }
            }
            this.cities=[...arr]
            
            
        }
    };

    parser(cities) {
        let firstPars= cities.split(';');

        let secondParse=[];

        for( let i=0; i<firstPars.length; i++){
            secondParse.push( firstPars[i].split(','))
        }
        for(let i=0; i<secondParse.length; i++){ 
            
            secondParse[i][0]=secondParse[i][0].slice(secondParse[i][0].indexOf('"')+1, secondParse[i][0].length)
            secondParse[i][1]=secondParse[i][1].slice(0, secondParse[i][0].indexOf('"')).trim()       
            secondParse[i][2]=+secondParse[i][2];
            secondParse[i][3]=+secondParse[i][3];
        }
        return secondParse
    };

    sort (searchOption) {
        switch (searchOption){
            case "N":
              this.cities.sort((a,b)=>{
                return b[2]-a[2]  
                })
                return this.cities[0][0]
            
            case "S":
                this.cities.sort((a,b)=>{
                    return a[2]-b[2]   
                    })
                    return this.cities[0][0]  

            case "W":
                this.cities.sort((a,b)=>{
                    return a[3]-b[3]   
                    })
                    return this.cities[0][0]  

            case "E":
                this.cities.sort((a,b)=>{
                    return b[3]-a[3]   
                    })
                    return this.cities[0][0]
        }
        
    };

    closest(lat, lng) {
        let distance=[];

        for(let i=0; i<this.cities.length; i++){
            let RadianLat1= lat*Math.PI/180;
            let RadianLat2= this.cities[i][2]*Math.PI/180;
            let RadianLng1= lng*Math.PI/180;
            let RadianLng2= this.cities[i][3]*Math.PI/180;
            let deltaLat=RadianLat1-RadianLat2;
            let deltaLng = RadianLng1-RadianLng2;


           
            let cos1 = Math.cos(RadianLat1);
            let cos2 =Math.cos(RadianLat2);
            
            let sinLat= Math.pow(Math.sin(deltaLat/2), 2)// квадрат синуса выражения (широта1- широта2)/2
            let sinLng =Math.pow(Math.sin(deltaLng/2), 2)
            let res = sinLat+cos1*cos2*sinLng
           
            let q = 2*Math.atan2(Math.sqrt(res),Math.sqrt(1-res))
            let d= q*6371// расстояние в км
            // console.log(d)
            let arr = [this.cities[i][0], d]
            distance.push(arr)

        }
        console.log(distance)
        distance.sort((a,b)=>{
           return a[1]-b[1] 
        })
        return distance[0][0]
    };

    getStates(){
        let states=[];
        for (let i=0; i<this.cities.length; i++){
            if (states.indexOf(this.cities[i][1]) === -1){
                states.push(this.cities[i][1])
            }
           
        }
        return states.join(' ')
    };

    searchByStates(state){
        let cities =[];

        for ( let i=0; i<this.cities.length; i++){
            if (this.cities[i][1].toUpperCase()=== state.toUpperCase()){
                cities.push(this.cities[i][0])
            }
        }
        return cities
    };

    add (city, state, lat,lng, ){
        this.cities.push([city, state, lat, lng])
    };

    saveToLocalStorage(){
        
        localStorage.setItem('cities', JSON.stringify( this.cities))
    };

    dataForViz(){
        let arr= this.getStates().split(' ');
        let data= {};
        for (let i=0; i<arr.length; i++){
            let sum =0;
            for (let b=0; b<this.cities.length; b++){
               
                if(arr[i]===this.cities[b][1]){
                    
                    ++sum
                }
                
            } 
            
            
            
            data[`${arr[i]}`]=sum

        }
        return data
    }

}

let Map = new CityMap ('"Nashville, TN", 36.17, -86.78; "New York, NY", 40.71, -74.00; "Atlanta, GA", 33.75, -84.39; "Denver, CO", 39.74, -104.98; "Seattle, WA", 47.61, -122.33; "Los Angeles, CA", 34.05, -118.24; "Memphis, TN", 35.15, -90.05');
console.log('its first console')
console.log(Map.cities)
// console.log(Map.sort("N"))
// console.log(Map.sort("S"))
// console.log(Map.sort("W"))
// console.log(Map.sort("E"))

console.log(Map.closest(40.50,-80.31))

console.log(Map.getStates())

console.log(Map.searchByStates("ny"))
console.log("now we add city")
Map.add("Pittsburgh", "PA",40.440624,-79.995888)
console.log(Map.cities)
Map.saveToLocalStorage()
console.log('its storage')
// console.log(Map.dataForViz())


let SelectSpan= document.getElementsByClassName('ShowMost'),
Select = document.getElementsByClassName("select"),
ShowBtn = document.getElementsByClassName("showBtn"),
ClosestLat=document.getElementsByClassName("closestLat"),
ClosestLng=document.getElementsByClassName("closestLng"),
ClosestCity =document.getElementsByClassName("closestCity"),
AddLat=document.getElementsByClassName("addLat"),
AddLng=document.getElementsByClassName("addLng"),
AddBtn=document.getElementsByClassName("addBtn"),
AddCity=document.getElementsByClassName("addCity"),
AddState=document.getElementsByClassName("addState"),
AddError= document.getElementsByClassName("addError"),
AllStates=document.getElementsByClassName('allStates'),
ShowStatets=document.getElementsByClassName('showStates'),
CitiesByState=document.getElementsByClassName("citiesByStates"),
SearchByStateInput=document.getElementsByClassName('searchByStateInput'),
SearchByStateBtn=document.getElementsByClassName('searchByStateBtn'),
Save=document.getElementsByClassName('save')



SelectSpan[0].innerText=Map.sort(Select[0].value)
Select[0].onchange= ()=>{
    SelectSpan[0].innerText=Map.sort(Select[0].value)
}
ShowBtn[0].onclick=()=>{
    
    let City = Map.closest(ClosestLat[0].valueAsNumber, ClosestLng[0].valueAsNumber);
    ClosestCity[0].innerText=City;
    
    
}
AddBtn[0].onclick =()=>{
    if(AddCity[0].value.length==0||AddState[0].value.length==0||AddLat[0].value.length==0||AddLng[0].value.length==0){
        AddError[0].innerText="please fill in all fields"
    }else{
        AddError[0].innerText=""
        Map.add(AddCity[0].value, AddState[0].value, AddLat[0].valueAsNumber, AddLng[0].valueAsNumber)
    }    
}

ShowStatets[0].onclick=()=>{
    AllStates[0].innerText=Map.getStates()
}

SearchByStateBtn[0].onclick=()=>{
    CitiesByState[0].innerText=Map.searchByStates(SearchByStateInput[0].value)
}
Save[0].onclick=()=>{
    Map.saveToLocalStorage()
}






let width = 450,
    height = 450,
    margin = 40


let radius = Math.min(width, height) / 2 - margin


let svg = d3.select("#my_dataviz")
  .append("svg")
    .attr("width", width)
    .attr("height", height)
  .append("g")
    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

// Create dummy data


let data =Map.dataForViz()
console.log("its data")


// set the color scale
let color = d3.scaleOrdinal()
  .domain(data)
  .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56"])

// Compute the position of each group on the pie:
let pie = d3.pie()
  .value(function(d) {return d.value; })
let data_ready = pie(d3.entries(data))

// Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
svg
  .selectAll('whatever')
  .data(data_ready)
  .enter()
  .append('path')
  .attr('d', d3.arc()
    .innerRadius(0)
    .outerRadius(radius)
  )
  .attr('fill', function(d){ return(color(d.data.key)) })
  .attr("stroke", "black")
  .style("stroke-width", "2px")
  .style("opacity", 0.7)